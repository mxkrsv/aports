# Contributor: Bradford D. Boyle <bradford.d.boyle@gmail.com>
# Maintainer: Bradford D. Boyle <bradford.d.boyle@gmail.com>
pkgname=hamlib
pkgver=4.5.1
pkgrel=0
pkgdesc="Library to control radio transceivers and receivers"
url="https://hamlib.github.io/"
arch="all"
license="LGPL-2.0-or-later"
makedepends="linux-headers perl-dev python3-dev swig tcl-dev chrpath libusb-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-perl py3-$pkgname:py3 $pkgname-tcl"
source="https://github.com/Hamlib/Hamlib/releases/download/$pkgver/hamlib-$pkgver.tar.gz"

build() {
	./configure \
		PYTHON=/usr/bin/python3 \
		--prefix=/usr \
		--sbindir=/usr/bin \
		--with-perl-binding \
		--with-python-binding \
		--with-tcl-binding
	make
}

package() {
	make DESTDIR="$pkgdir" install
	rm -f "$pkgdir"/usr/lib/*.a
	rm -f "$pkgdir"/usr/lib/tcl8.6/Hamlib/hamlibtcl.a

	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete
	chrpath --delete "$pkgdir"/usr/lib/perl5/site_perl/auto/Hamlib/Hamlib.so
	rm -f "$pkgdir"/usr/lib/perl5/site_perl/perltest.pl
	rm -Rf "$pkgdir"/usr/lib/perl5/core_perl
	mv "$pkgdir"/usr/lib/perl5/site_perl "$pkgdir"/usr/lib/perl5/vendor_perl
}

perl() {
	pkgdesc="Perl bindings for $pkgname"
	depends="perl"

	amove usr/lib/perl5
}

py3() {
	pkgdesc="Python3 bindings for $pkgname"
	depends="python3"

	amove usr/lib/python3*
}

tcl() {
	pkgdesc="Tcl bindings for $pkgname"

	amove usr/lib/tcl8.6
}

sha512sums="
3fbfa18f3f04890eb0bb2ffef5347ffc3759fa276d613785a0e585dd3613837db4461eb79bf7e3794f6ce33dcae8a4357b3f27c8be44a9d838fcb82cb0a60753  hamlib-4.5.1.tar.gz
"
