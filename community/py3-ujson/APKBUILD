# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-ujson
pkgver=5.6.0
pkgrel=0
pkgdesc="Fast Python JSON encoder and decoder"
url="https://github.com/ultrajson/ultrajson"
arch="all"
license="BSD-3-Clause"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-pytest
	py3-tz
	"
source="https://pypi.io/packages/source/u/ujson/ujson-$pkgver.tar.gz"
builddir="$srcdir/ujson-$pkgver"

# secfixes:
#   5.4.0-r0:
#     - CVE-2022-31116
#     - CVE-2022-31117
#   5.2.0-r0:
#     - CVE-2021-45958

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
996c9746e011f116fcab3e61345df27cb2c1251852f758eff1d3465e19df03deb1a037669aa6ee5aad2fc6c2cf76c22075ad6a73158ff4dc81357e59a4ee244a  ujson-5.6.0.tar.gz
"
