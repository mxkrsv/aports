# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpublictransport
pkgver=22.12.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/libraries/kpublictransport"
pkgdesc="Library to assist with accessing public transport timetables and other data"
license="BSD-3-Clause AND LGPL-2.0-or-later AND MIT"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	zlib-dev
	protobuf-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpublictransport-$pkgver.tar.xz"
options="!check" # Broken for now
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
570346ee94c24d1ba88af9f3595e89bf18931f020f0265d8f0e32c329d23b4b6d2c7ef91b210ebf702245f80c4252001e1651744d38d7e1ed32b39053f425f6c  kpublictransport-22.12.0.tar.xz
"
