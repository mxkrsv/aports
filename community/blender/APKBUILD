# Contributor: Mark Riedesel <mark@klowner.com>
# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=blender
pkgver=3.4.0
_pkgver=${pkgver%.[0-9]}
pkgrel=1
pkgdesc="3D Creation/Animation/Publishing System"
url="https://www.blender.org/"
arch="x86_64 aarch64" # limited by openvdb
license="GPL-2.0-or-later"
depends="blender-shared=$pkgver-r$pkgrel"
makedepends="cmake
	alembic-dev
	blosc-dev
	boost-dev
	eigen-dev
	embree-dev
	embree-static
	ffmpeg-dev
	fftw-dev
	freetype-dev
	gmp-dev
	jack-dev
	libdecor-dev
	libepoxy-dev
	libharu-dev
	libjpeg-turbo-dev
	libpng-dev
	libsndfile-dev
	libtbb-dev
	libx11-dev
	libxi-dev
	libxkbcommon-dev
	libxrender-dev
	llvm-dev
	lzo-dev
	openal-soft-dev
	opencolorio-dev
	openexr-dev
	openimageio-dev
	openjpeg-dev
	opensubdiv-dev
	openvdb-dev
	openvdb-nanovdb
	openxr-dev
	osl
	osl-dev
	potrace-dev
	pugixml-dev
	pulseaudio-dev
	py3-numpy-dev
	python3-dev
	samurai
	sdl2-dev
	tiff-dev
	wayland-dev
	wayland-protocols
	"
case "$CARCH" in
	"x86_64") makedepends="$makedepends openimagedenoise-dev";;
esac
subpackages="$pkgname-doc $pkgname-shared::noarch $pkgname-headless py3-$pkgname:python"
source="https://download.blender.org/source/blender-$pkgver.tar.xz
	0001-musl-fixes.patch
	0002-increase-thread-stack-size-for-musl.patch
	"

# secfixes:
#   3.3.0-r0:
#     - CVE-2022-2831
#     - CVE-2022-2832
#     - CVE-2022-2833

build() {
	# Headless
	mkdir -p "$builddir"/build-headless
	cd "$builddir"/build-headless
	_build -C../build_files/cmake/config/blender_headless.cmake

	# Full
	mkdir -p "$builddir"/build-full
	cd "$builddir"/build-full
	_build -C../build_files/cmake/config/blender_full.cmake

	# Python module
	mkdir -p "$builddir"/build-py
	cd "$builddir"/build-py
	_build -C../build_files/cmake/config/bpy_module.cmake
}

_build() {
	local _py_version=$(python3 -c 'import sys; print("%i.%i" % (sys.version_info.major, sys.version_info.minor))')

	export CFLAGS="$CFLAGS -D__MUSL__"
	export CXXFLAGS="$CXXFLAGS -D__MUSL__"

	cmake .. "$@" \
		-G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DWITH_PYTHON_INSTALL=OFF \
		-DWITH_INSTALL_PORTABLE=OFF \
		-DWITH_SYSTEM_LZO=ON \
		-DWITH_SYSTEM_EIGEN3=ON \
		-DPYTHON_VERSION=$_py_version

	cmake --build .
}

package() {
	# Install headless files
	cd "$builddir"/build-headless
	DESTDIR="$pkgdir"/headless cmake --install .

	# Rename the headless blender to blender-headless
	mkdir -p "$pkgdir"/usr/bin
	mv "$pkgdir"/headless/usr/bin/blender "$pkgdir"/usr/bin/blender-headless
	rm -rf "$pkgdir"/headless

	# Install python module
	cd "$builddir"/build-py
	DESTDIR="$pkgdir" cmake --install .

	# Install the full package
	cd "$builddir"/build-full
	DESTDIR="$pkgdir" cmake --install .
}

shared() {
	pkgdesc="Blender shared runtime data and add-on scripts"
	depends=""
	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/blender "$subpkgdir"/usr/share/
}

headless() {
	pkgdesc="$pkgdesc (headless build)"
	depends="blender-shared=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/blender-headless "$subpkgdir"/usr/bin/
}

python() {
	local _py_version=$(python3 -c 'import sys; print("%i.%i" % (sys.version_info.major, sys.version_info.minor))')

	pkgdesc="Blender modules for Python 3"

	mkdir -p "$subpkgdir"/usr/lib/python"$_py_version"/site-packages

	# temporary fix, while the build script has a bug
	mv "$pkgdir"/usr/lib/python"$_py_version"/site-packages/bpy/__init__.so "$subpkgdir"/usr/lib/python"$_py_version"/site-packages/bpy.so
	# mv "$pkgdir"/usr/lib/python"$_py_version"/site-packages/bpy.so "$subpkgdir"/usr/lib/python"$_py_version"/site-packages/

	rm -rf "$pkgdir"/usr/lib

	# Symlink to the blender-shared files
	ln -s /usr/share/blender/"$_pkgver" "$subpkgdir"/usr/lib/python"$_py_version"/site-packages/"$_pkgver"
}

sha512sums="
da81c9e34a0f9a34568680dd73d88ad89292d3915ce0a6a2cfd773a6d68ade650ad220abf73d5c1c06a520aab76c1926ecc4ad2755492e28388f44ca153353a9  blender-3.4.0.tar.xz
7940236b3f0d57ecf3e7fc1bf8db5a5d143d9df53160afe36b42a3e5fc5e8023fe00bf6a27cbee0173c7a38c2023ad7be0bd1dae8361aa290f661a4a688ac43d  0001-musl-fixes.patch
0edaa0367903cf14103dd9695f9ef31657c5d1e9c67feed3479ce0801a62db9eb80f6ffaa49126948ddd841e060727e8046b77d1c3b00ddb4a4cd75b46b1386a  0002-increase-thread-stack-size-for-musl.patch
"
