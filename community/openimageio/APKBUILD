# Contributor: Mark Riedesel <mark@klowner.com>
# Contributor: Damian Kurek <starfire24680@gmail.com>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=openimageio
pkgver=2.4.6.1
pkgrel=2
pkgdesc="Image I/O library supporting a multitude of image formats"
options="!check" # more than 10% of all tests fail
url="https://sites.google.com/site/openimageio/"
# s390x has missing dependency ptex-dev
arch="all !s390x"
license="BSD-3-Clause"
makedepends="cmake
	boost-dev
	bzip2-dev
	ffmpeg-dev
	fmt-dev
	freetype-dev
	giflib-dev
	hdf5-dev
	libheif-dev
	libtbb-dev
	libraw-dev
	libwebp-dev
	mesa-dev
	opencolorio-dev
	openexr-dev
	openjpeg-dev
	ptex-dev
	ptex-static
	python3-dev
	py3-pybind11-dev
	qt5-qtbase-dev
	robin-map
	samurai
	tiff-dev
	"
subpackages="py3-$pkgname:_python $pkgname-dev $pkgname-doc $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenImageIO/oiio/archive/v$pkgver.tar.gz
	0001-Disable-ARM-NEON-support.patch
	"
builddir="$srcdir/oiio-$pkgver"

build() {
	local _py_version=$(python3 --version | cut -c 8-11)
	local _iv="ON"

	case "$CARCH" in
		aarch64|armv7|armhf)
			_iv="OFF";;
	esac

	# fails to build with fortify source enabled
	export CXXFLAGS="$CXXFLAGS -U_FORTIFY_SOURCE"

	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_SKIP_RPATH=ON \
		-DBUILD_TESTING=OFF \
		-DSTOP_ON_WARNING=OFF \
		-DENABLE_iv=$_iv \
		-DINSTALL_FONTS=OFF
	cmake --build build
}

check() {
	cd build
	ctest -E broken
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

tools() {
	pkgdesc="Tools for manipulating a multitude of image formats"

	amove usr/bin
}

_python() {
	local pyver="${subpkgname:2:1}"
	pkgdesc="Python $pyver bindings for OpenImageIO image I/O library"
	depends="python$pyver"

	amove usr/lib/python*
}

sha512sums="
b151be99f2124a81f8d7e34d48639f44c6d5bd4e8cdfb2bebce1daa17471bc3337b614539676fb051bd1f3deee833c2372e20622aec798512c10f61e28bf8f0d  openimageio-2.4.6.1.tar.gz
3dfa04140a7e46ae42c16a20026ff41003bf2ea33fdfa2a1a21dc851e5bfdbebb0eb6227102a5aab6f07cee7e292eeb6838d7a74a62325678d64efe7b766c47f  0001-Disable-ARM-NEON-support.patch
"
