# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khangman
pkgver=22.12.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://edu.kde.org/khangman"
pkgdesc="Hangman game"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kxmlgui-dev
	libkeduvocdocument-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/khangman-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
767dcee0108c247bfb31bb35ce8a02b2cab62acbf5510b2559e7943c89a5cae65de942a45acb19bc5f4d9f9c9fbbddca6c4ef6592535492dce587a0112e68dec  khangman-22.12.0.tar.xz
"
