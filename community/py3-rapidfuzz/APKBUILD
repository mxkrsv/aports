# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-rapidfuzz
pkgver=2.13.6
pkgrel=0
pkgdesc="Rapid fuzzy string matching in Python using various string metrics"
url="https://github.com/maxbachmann/RapidFuzz"
arch="all"
license="MIT"
makedepends="
	cmake
	cython
	py3-gpep517
	py3-rapidfuzz-capi
	py3-scikit-build
	py3-setuptools
	python3-dev
	samurai
	"
checkdepends="
	py3-hypothesis
	py3-numpy
	pytest
	"
source="https://files.pythonhosted.org/packages/source/r/rapidfuzz/rapidfuzz-$pkgver.tar.gz"
builddir="$srcdir/rapidfuzz-$pkgver"

case "$CARCH" in
x86*)
	# float rounding
	options="$options !check"
	;;
esac

build() {
	RAPIDFUZZ_BUILD_EXTENSION=1 \
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer \
		dist/rapidfuzz*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/rapidfuzz*.whl
}

sha512sums="
3255d92a5eccf15577572a0da9075f092a3a33a2d37d84da27389914cdec4e97b3314d9dc8d373b265824e33c0e3522f8ac8888eef6b36b3cab55d41e11708b2  rapidfuzz-2.13.6.tar.gz
"
