# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=k3b
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/multimedia/org.kde.k3b"
pkgdesc="A full-featured CD/DVD/Blu-ray burning and ripping application"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	cdrdao
	dvd+rw-tools
	libburn
	"
makedepends="
	extra-cmake-modules
	flac-dev
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kservice-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	lame-dev
	libdvdread-dev
	libkcddb-dev
	libmad-dev
	libsamplerate-dev
	libvorbis-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	solid-dev
	taglib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/k3b-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DK3B_BUILD_MUSE_DECODER_PLUGIN=OFF \
		-DK3B_BUILD_SNDFILE_DECODER_PLUGIN=OFF \
		-DK3B_ENABLE_MUSICBRAINZ=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d9bce4047642d504b6271a2c234fa1c9df8c32085b59d423e834dac3ad333d609d4881e084131cc508213a264f4a164d0319a18c7691e783afe851c4c184c454  k3b-22.12.0.tar.xz
"
