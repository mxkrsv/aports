# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkexiv2
pkgver=22.12.0
pkgrel=0
pkgdesc="A library to manipulate pictures metadata"
url="https://www.kde.org/applications/graphics"
arch="all !armhf" # extra-cmake-modules
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev exiv2-dev samurai"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkexiv2-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
5018360c4c5611660009977890d072030c6086e5a0e48880059a183db0ebe1acbf2039b106835242f39d57f1675f9abd067d5d33c6eccb311bede889a62be65e  libkexiv2-22.12.0.tar.xz
"
