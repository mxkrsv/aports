# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kompare
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/org.kde.kompare"
pkgdesc="Graphical File Differences Tool"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	kiconthemes-dev
	kjobwidgets-dev
	kparts-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	libkomparediff2-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kompare-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a0193931196d17e43e2de1da36c15e0739cf28ed45fb7f5e1fd7de3307e172117ae06f9bd95a16e7f34cd8995c4943786401c209c8c694cc65d774541c79db07  kompare-22.12.0.tar.xz
"
