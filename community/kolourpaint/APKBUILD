# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kolourpaint
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="http://www.kolourpaint.org/"
pkgdesc="An easy-to-use paint program"
license="BSD-2-Clause AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdelibs4support-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libksane-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kolourpaint-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9ba97f85cf795f381416b3c435bae20204153d27e6173bf85c01eea2f7560ecc8b5c5da43dac355affac6953393ebf7ebdeda165debe8a0842cad8c61c01107b  kolourpaint-22.12.0.tar.xz
"
