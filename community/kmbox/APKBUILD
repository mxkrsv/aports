# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmbox
pkgver=22.12.0
pkgrel=0
pkgdesc="Library for accessing mail storages in MBox format"
# armhf blocked by extra-cmake-modules
# s390x blocked by kmime
arch="all !armhf !s390x"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="kmime-dev"
makedepends="$depends_dev extra-cmake-modules samurai"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmbox-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
6be48f7b0aaf9c4d9013bf2e83129a817ee179a76712762d4d8a4dc1f7dca711c354fc9c285de17f1afa9b040151f19b004a924ad629e487bc315bb706c06c8b  kmbox-22.12.0.tar.xz
"
