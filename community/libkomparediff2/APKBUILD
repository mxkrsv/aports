# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkomparediff2
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development"
pkgdesc="Library to compare files and strings"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkomparediff2-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ad5b2324e6276fccde5d68c445aa294c6fdf204e075b39a20344d51996ae57368466df0573eaf0f00480557346115c22c49be53c40d79dee1486b78ec896efc3  libkomparediff2-22.12.0.tar.xz
"
