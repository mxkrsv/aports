# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kidentitymanagement
pkgver=22.12.0
pkgrel=0
pkgdesc="KDE PIM libraries"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kpimtextedit-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kidentitymanagement-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kpimidentity-signaturetest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kpimidentity-signaturetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d107e805f733e91f60b7c2070aa154ca2b66a4f60999f5dc8163a78e2d677578bafd45c11cb16b08c12460f1c194e2a319c9cb6197500d2eb4b5603a2cbda1f8  kidentitymanagement-22.12.0.tar.xz
"
